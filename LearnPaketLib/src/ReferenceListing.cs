﻿using System.IO;
using System.Reflection;


namespace FmgSoftware.LearnPaketLib
{
    public static class ReferenceListing
    {
        public static void PrintReferences(TextWriter writer)
        {
            foreach(var assemblyName in Assembly.GetExecutingAssembly().GetReferencedAssemblies())
            {
                writer.WriteLine("{0} - {1}", assemblyName.Name, assemblyName.Version);
            }
        }
    }
}